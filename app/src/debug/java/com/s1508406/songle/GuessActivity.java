package com.s1508406.songle;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

public class GuessActivity extends AppCompatActivity {
private String title;
private String url;
private EditText editText;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_guess);
        editText = (EditText) findViewById(R.id.editText);
        //gets url and title
        url = getIntent().getExtras().getString("URL");//getting the url from Song.activity
        title = getIntent().getExtras().getString("Title");//getting the title ^^
    }

    public void guessAttempt(View view){ //when guess button is clicked
        String guess = editText.getText().toString();//gets the user guess
        if(guess.toLowerCase().equals(title.toLowerCase())) {//if the string matches the title then has a browser intent on the Url.
            Intent browser = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
            startActivity(browser);
        }else{
            Toast.makeText(getApplicationContext(),"Incorrect Guess",Toast.LENGTH_SHORT).show();//message if the guess if incorrect
        }
    }
}
