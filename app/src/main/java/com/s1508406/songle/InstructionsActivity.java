package com.s1508406.songle;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.method.ScrollingMovementMethod;
import android.widget.TextView;

public class InstructionsActivity extends AppCompatActivity {
    private TextView textView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_instructions);
        textView = (TextView) findViewById(R.id.instuctionsView); //Hardcoded instructions and put they are put in a textview in the activity_instructions.
        String input = "1. Songle is a game that requires the user to walk around the map and collect words in order to guess songs \n" + "\n" +
                "2. To play, select the song and the difficulty from the \"Show Songs\" Menu \n" + "\n" +
                "3. There are 5 difficulties, the harder have fewer words and variation in marker \n" + "\n" +
                "4. Markers signify how useful a word is, yellower or empty markers mean they are less useful \n" + "\n" +
                "5. If you're stuck, press the hint button. It will cost you 50 points and give you 1 random word \n" + "\n" +
                "6. For each word you collect, you will receiver 10 points. In order to get hint, you must collect at least 1 word in that song \n" + "\n" +
                "7. Once you hve guessed a song, you can continue to collect markers to collect more points.";

        textView.setMovementMethod(new ScrollingMovementMethod());
        textView.setText(input);
    }
}
