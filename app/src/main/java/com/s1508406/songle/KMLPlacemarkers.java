package com.s1508406.songle;

import java.util.ArrayList;

/**
 * Created by sidchan on 11/12/2017.
 */

//PlaceMarker Parsing class

public class KMLPlacemarkers //class is basicallt for maping of the kml file, this is the root
{
    private Kml kml;//root has only 1 variable, Kml of type Kml
    Kml getKml() { return this.kml; }
    public void setKml(Kml kml) { this.kml = kml; }
}


class Point //point is a variable in Placemark, only the string coordinates
{
    private String coordinates;
    String getCoordinates() { return this.coordinates; }
    public void setCoordinates(String coordinates){
        this.coordinates = coordinates;
    }
}

class Placemark//placemark has many variables, name, description and point
{
    private String name;
    public String getName() { return this.name; }
    public void setName(String name) { this.name = name; }
    private String description;
    public String getDescription() { return this.description; }
    public void setDescription(String description) { this.description = description; }

    private Point Point;

    Point getPoint() {
        return this.Point;
    }

    public void setPoint(Point Point) {
        this.Point = Point;
    }
}

class Document //document is the superclass that hold allthe Placemarks, it has an array of placemerks
{

    private ArrayList<Placemark> Placemark;

    ArrayList<Placemark> getPlacemark() { return this.Placemark; }

    public void setPlacemark(ArrayList<Placemark> Placemark) { this.Placemark = Placemark; }
}

class Kml //kml is the superclass containing document
{
    private String xmlns;
    public String getXmlns() { return this.xmlns; }
    public void setXmlns(String xmlns) { this.xmlns = xmlns; }

    private Document Document;
    Document getDocument() { return this.Document; }
    public void setDocument(Document Document) { this.Document = Document; }
}
