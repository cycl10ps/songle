package com.s1508406.songle;

import android.Manifest;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.TextView;


public class MainActivity extends AppCompatActivity {
    private NetworkReceiver receiver = new NetworkReceiver();
    public static String networkPref = null;
    public static final String WIFI = "Wi-Fi";
    public static final String ANY = "Any";
    private int points;
    private TextView textView;
    private int markers;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        IntentFilter filter = new
                IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION);
//        receiver = new NetworkReceiver();
//        this.registerReceiver(receiver,filter);
        setContentView(R.layout.activity_main);

        loadData();  //Loading relevant data from Shared preferences

        textView = (TextView) findViewById(R.id.mainPoints); //set the points value in the textview
        textView.setText(String.valueOf(points));

        ActivityCompat.requestPermissions(this,new String[]
                {Manifest.permission.ACCESS_FINE_LOCATION}, 1); //request for location services if they are not allowedfor the app;


    }
    public void loadData(){ //Loads in the points value
        SharedPreferences sharedPreferences = getSharedPreferences("shared_prefs", MODE_PRIVATE);
        points = sharedPreferences.getInt("points", 30);

    }
//
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case 1: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                } else {
                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                }
                return;
            }
            // other 'case' lines to check for other
            // permissions this app might request
        }
    }

    public void showInstruction(View view){ // goes to the InstructionActivity
        Intent newActivity = new Intent(this, InstructionsActivity.class);
        startActivity(newActivity);
    }

    public void showSongs(View view){ //goes to the SongsActivity
        Intent newActivity = new Intent(this, SongsActivity.class);
        startActivity(newActivity);
        finish();
    }

    public class NetworkReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            ConnectivityManager connMgr = (ConnectivityManager)
                    context.getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
            if (networkPref.equals(WIFI) && networkInfo != null
                    && networkInfo.getType() ==
                    ConnectivityManager.TYPE_WIFI) {
// Wi´Fi is connected, so use Wi´Fi
            } else if (networkPref.equals(ANY) && networkInfo != null) {
// Have a network connection and permission, so use data
            } else {
// No Wi´Fi and no permission, or no network connection
            }
        }
    }





}


