package com.s1508406.songle;

import android.Manifest;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.location.Location;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONException;
import org.json.JSONObject;
import org.json.XML;
import org.xmlpull.v1.XmlPullParserException;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.lang.reflect.Type;
import java.net.URL;
import java.util.ArrayList;


public class MapsActivity extends FragmentActivity implements OnMapReadyCallback, GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener,
        LocationListener {

    private GoogleMap mMap;
    private GoogleApiClient mGoogleApiClient;
    private final int PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION = 1;
    private boolean mLocationPermissionGranted = false;
    private Location mLastLocation;
    private static final String TAG = "MapsActivity";
    private InputStreamReader inputStream;
    private KMLPlacemarkers placemarkers;
    private Bitmap bitmapFactoryuc;
    private Bitmap bitmapFactorybo;
    private Bitmap bitmapFactorynb;
    private Bitmap bitmapFactoryin;
    private Bitmap bitmapFactoryvi;
    private String song;
    private String lvl;
    private ArrayList<Placemark> found = new ArrayList<Placemark>();
    private double lati;
    private double lont;
    ArrayList<Placemark> forFound;
    private int points;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.

        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        mapFragment.getMapAsync(this);
// Create an instance of GoogleAPIClient.
        if (mGoogleApiClient == null) {
            mGoogleApiClient = new GoogleApiClient.Builder(this)
                    .addConnectionCallbacks(this)
                    .addOnConnectionFailedListener(this)
                    .addApi(LocationServices.API)
                    .build();
        }
        song = getIntent().getExtras().getString("song"); //get songs name
        lvl = getIntent().getExtras().getString("lvl");  //get lvl user chose
        loadData();


        new KMLTask().execute();//executs async task kmltast


    }

        public void saveData(){//saves the lists found, forFound and the int points
        SharedPreferences sharedPreferences = getSharedPreferences("shared_prefs", MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        Gson gson = new Gson();
        String json = gson.toJson(found);
        editor.putString(song, json);
        editor.apply();
        json = gson.toJson(forFound);
        editor.putString(song+"all", json);
        editor.apply();
        editor.putInt("points", points);
        editor.apply();

    }

        public void loadData(){ //loads in the points and also the found list

        SharedPreferences sharedPreferences = getSharedPreferences("shared_prefs", MODE_PRIVATE);
        points = sharedPreferences.getInt("points",0);
        Gson gson = new Gson();
        String json = sharedPreferences.getString(song, null);
        Type type = new TypeToken<ArrayList<Placemark>> () {}.getType();
        found = gson.fromJson(json, type);

        if(found == null){
            Log.d("LOGNULLTEST", "FOUND IS EMPTY");
            found = new ArrayList<Placemark>();
        }



    }

    private class KMLTask extends AsyncTask<Void,String,KMLPlacemarkers> { //asynctast, getkml stream the the bitmap initation have to be  done in the background

        @Override
        protected KMLPlacemarkers doInBackground(Void... voids) {

            try {
                getKMLStream();
                bitmapFactoryuc = BitmapFactory.decodeStream(new URL("http://maps.google.com/mapfiles/kml/paddle/wht-blank.png").openStream()); //these are needed for the marker type
                bitmapFactorybo = BitmapFactory.decodeStream(new URL("http://maps.google.com/mapfiles/kml/paddle/ylw-blank.png").openStream());
                bitmapFactorynb = BitmapFactory.decodeStream(new URL("http://maps.google.com/mapfiles/kml/paddle/ylw-circle.png").openStream());
                bitmapFactoryin = BitmapFactory.decodeStream(new URL("http://maps.google.com/mapfiles/kml/paddle/orange-diamond.png").openStream());
                bitmapFactoryvi = BitmapFactory.decodeStream(new URL("http://maps.google.com/mapfiles/kml/paddle/red-stars.png").openStream());


            } catch (IOException e) {
                e.printStackTrace();
            } catch (XmlPullParserException e) {
                e.printStackTrace();
            }

            return placemarkers;
        }

        @Override
        protected  void onPostExecute(KMLPlacemarkers kmlpm) {
            ArrayList<Placemark> pms = placemarkers.getKml().getDocument().getPlacemark(); //gets the list of placemarkers and stores it in pms
            String lat;
            String lon;
            String coords;

            boolean test = false; //for comparring
            for (int i = 0; i < found.size(); i++) {//removes all the markers in found from pms
                for(int j = 0; j < pms.size(); j++){
                    if(pms.get(j).getName().equals(found.get(i).getName())){
                        pms.remove(j);
                    }
                }
            }
            forFound = pms;//sets forFound to pms for use when a marker is selected
            for (int i = 0; i < pms.size(); i++) {//loops through all the placemarks in pms


                    coords = pms.get(i).getPoint().getCoordinates(); //get the coordinates int he form of 1 string
                    lon = coords.substring(0, pms.get(i).getPoint().getCoordinates().indexOf(','));//use substrings to get the correct values
                    lat = coords.substring(coords.indexOf(',') + 1, coords.indexOf(',', coords.indexOf(',') + 1));

                //basically checks descrititon to seethe type of point,converts the
                //the lat and lon strings into doubles and makes them LatLngs and not strings
                //gets the correct bitmapFactory to load in the correct marker type
                    if (pms.get(i).getDescription().equals("boring")) {//basically checks descrititon to seethe type of point,converts the
                        mMap.addMarker(new MarkerOptions().position(new LatLng(Double.parseDouble(lat), Double.parseDouble(lon))).title(pms.get(i).getName()).icon(BitmapDescriptorFactory.fromBitmap(bitmapFactorybo)));
                    } else if (pms.get(i).getDescription().equals("notboring")) {
                        mMap.addMarker(new MarkerOptions().position(new LatLng(Double.parseDouble(lat), Double.parseDouble(lon))).title(pms.get(i).getName()).icon(BitmapDescriptorFactory.fromBitmap(bitmapFactorynb)));
                    } else if (pms.get(i).getDescription().equals("interesting")) {
                        mMap.addMarker(new MarkerOptions().position(new LatLng(Double.parseDouble(lat), Double.parseDouble(lon))).title(pms.get(i).getName()).icon(BitmapDescriptorFactory.fromBitmap(bitmapFactoryin)));
                    } else if (pms.get(i).getDescription().equals("veryinteresting")) {
                        mMap.addMarker(new MarkerOptions().position(new LatLng(Double.parseDouble(lat), Double.parseDouble(lon))).title(pms.get(i).getName()).icon(BitmapDescriptorFactory.fromBitmap(bitmapFactoryvi)));
                    } else {
                        mMap.addMarker(new MarkerOptions().position(new LatLng(Double.parseDouble(lat), Double.parseDouble(lon))).title(pms.get(i).getName()).icon(BitmapDescriptorFactory.fromBitmap(bitmapFactoryuc)));
                    }

            }



            mMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() { //when a markeris selection
                @Override
                public boolean onMarkerClick(Marker marker) {
                    Placemark temp = new Placemark();

                    Location loc1 = new Location(""); //gets users last known position
                    loc1.setLongitude(lont);
                    loc1.setLatitude(lati);

                    Location loc2 = new Location("");//gets marker position
                    loc2.setLatitude(marker.getPosition().latitude);
                    loc2.setLongitude(marker.getPosition().longitude);

                    if(loc1.distanceTo(loc2) <= 20) {//if distance is less that 20
                        //find the marker in forFound  and adds it to found. also adds 10 points for finding a point, also removes that placemark from forFound
                        for (int i = 0; i < forFound.size(); i++) {
                            if (forFound.get(i).getName().equals(marker.getTitle())) {
                                found.add(forFound.get(i));
                                forFound.remove(i);
                                points = points + 10;
                                break;
                            }
                        }

                        saveData();//saves the data
                        marker.remove();//removes marker
                        Toast.makeText(getApplicationContext(),"Word Collected",Toast.LENGTH_SHORT).show();//message saying word collected
                    }else{
                        Toast.makeText(getApplicationContext(),"Please Move Closer to the Marker",Toast.LENGTH_SHORT).show();//if the marker is too far, message saying move cloer to marker
                    }
                    return true;
                }
            });


        }
    }

    public void getKMLStream() throws IOException, XmlPullParserException {

        String kmlURL = "http://www.inf.ed.ac.uk/teaching/courses/selp/data/songs/" + song.substring(song.length()-2,song.length()) + "/map" + lvl + ".txt"; //set the url using song and lvl
        StringBuilder stringBuilder = new StringBuilder();
        String line;
        BufferedReader bufferedReader;
        try{ //parses the file into an input stream
            inputStream = new InputStreamReader(new URL(kmlURL).openStream());
            bufferedReader = new BufferedReader(inputStream);

            while((line = bufferedReader.readLine()) != null ){
                stringBuilder.append(line);
            }

        }catch(IOException e){
            e.printStackTrace();

        }

        String xml = stringBuilder.toString(); //set xml to the xml string which is the xml file

        JSONObject jsonObject = null;
        try{
            jsonObject = XML.toJSONObject(xml); //make the xml string a json object
        } catch (JSONException e) {
            e.printStackTrace();
        }
        String json = jsonObject.toString();
        Gson gson = new Gson();
        placemarkers = gson.fromJson(json, KMLPlacemarkers.class); //use gson parser make the whole list in the format in KMLPlacemarksers


    }


    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) { //
        mMap = googleMap;

        try {
// Visualise current position with a small blue circle
            mMap.setMyLocationEnabled(true);
        } catch (SecurityException se) {
            System.out.println("Security exception thrown [onMapReady]");
        }
// Add ‘‘My location’’ button to the user interface
        mMap.getUiSettings().setMyLocationButtonEnabled(true);
    }

    @Override
    protected void onStart() {
        super.onStart();
        mGoogleApiClient.connect();
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (mGoogleApiClient.isConnected()) {
            mGoogleApiClient.disconnect();
        }
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        try { createLocationRequest(); }
        catch (java.lang.IllegalStateException ise) {
            System.out.println("IllegalStateException thrown [onConnected]");
        }
// Can we access the user’s current location?
        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_FINE_LOCATION) ==
                PackageManager.PERMISSION_GRANTED) {
            mLastLocation =
                    LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient); //check permissions
        } else {
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                    PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION);//requests permissions
        }
    }

    @Override
    public void onConnectionSuspended(int i) {
        System.out.println(" >>>> onConnectionSuspended");
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        // An unresolvable error has occurred and a connection to Google APIs
// could not be established. Display an error message, or handle
// the failure silently
        System.out.println("             >>>> onConnectionFailed");

    }

    @Override
    public void onLocationChanged(Location current) {//whenlocation changed updates lati and lont as well
        lati = current.getLatitude();
        lont = current.getLongitude();
        System.out.println(
                "[onLocationChanged] Lat/long now (" +
                String.valueOf(current.getLatitude()) + "," +
                String.valueOf(current.getLongitude()) + "("
);


    }
    @Override
    public void onBackPressed(){//starts Songs activity to ensure points is updated
        finish();
        startActivity(new Intent(this,SongsActivity.class));//restart Songs to update points.
    }

    protected void createLocationRequest() { //requests current location  with a LocationRequest
// Set the parameters for the location request
        LocationRequest mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(5000); // preferably every 5 seconds
        mLocationRequest.setFastestInterval(1000); // at most every second
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);

        // Can we access the user’s current location?
        int permissionCheck = ContextCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_FINE_LOCATION);
        if (permissionCheck == PackageManager.PERMISSION_GRANTED ) {
            LocationServices.FusedLocationApi.requestLocationUpdates(
                    mGoogleApiClient, mLocationRequest, this);//permission checks
        }
    }




}
