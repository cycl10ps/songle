package com.s1508406.songle;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Xml;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Type;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Random;

public class SongsActivity extends AppCompatActivity {
    public static ArrayList<Song> songs = new ArrayList<Song>();
    public DownloadXmlTask dxt = new DownloadXmlTask();
    private Spinner mySpinner;
    private Spinner lvlSpinner;
    ArrayList<String> songSpin;
    private int points;
    private TextView textView;
    private ArrayList<Placemark> forHints;
    private ArrayList<Placemark> found;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_songs);
        loadData();
        textView = (TextView) findViewById(R.id.songsPoints);
        textView.setText(String.valueOf(points));
        dxt.execute("http://www.inf.ed.ac.uk/teaching/courses/selp/data/songs/songs.xml");

        mySpinner = (Spinner) findViewById(R.id.spinner1);

        lvlSpinner = (Spinner) findViewById(R.id.spinner2);

        ArrayAdapter<String> lvlAdapter = new ArrayAdapter<String>(SongsActivity.this, R.layout.spinner_layout, getResources().getStringArray(R.array.lvls));
        lvlAdapter.setDropDownViewResource((android.R.layout.simple_spinner_dropdown_item));
        lvlSpinner.setAdapter(lvlAdapter);
    }

    public void saveData(){ //saves the relevant data
        SharedPreferences sharedPreferences = getSharedPreferences("shared_prefs", MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();

        editor.putInt("points", points); //saving changes to points(hints)
        editor.apply();

        Gson gson = new Gson(); //saves changes to found(important) so that it removes the marker and also shows the word in
        String json = gson.toJson(found);
        editor.putString(mySpinner.getSelectedItem().toString(), json);
        editor.apply();

        json = gson.toJson(forHints);//saves forHints, this is only to ensure hints do not show the same word over and over.
        editor.putString(mySpinner.getSelectedItem()+"all", json);
        editor.apply();


    }

    public void loadData(){
        SharedPreferences sharedPreferences = getSharedPreferences("shared_prefs", MODE_PRIVATE);
        points = sharedPreferences.getInt("points",0);
        //only need to get points in this function

    }

    public static void setSongs(ArrayList<Song> s){
        songs = s;
    }


    public static ArrayList<Song> getSongs(){
        return songs;
    }


    public void showMap(View view){ //switched to map view

        Intent mapActivity = new Intent(this, MapsActivity.class);
        mapActivity.putExtra("song", mySpinner.getSelectedItem().toString());//sends which song is needed.
        String lvl = "";
        if(lvlSpinner.getSelectedItem().equals("Novice")){ //converts spinners into map number(1 is harders, 5 is easiest on the maps)
            lvl = "5";
        }else if(lvlSpinner.getSelectedItem().equals("Amateur")){
            lvl = "4";
        }else if(lvlSpinner.getSelectedItem().equals("Adept")){
            lvl = "3";
        }else if(lvlSpinner.getSelectedItem().equals("Master")){
            lvl = "2";
        }else if(lvlSpinner.getSelectedItem().equals("Legend")){
            lvl = "1";
        }

        mapActivity.putExtra("lvl", lvl);

        startActivity(mapActivity);
        finish();
    }

    @Override
    protected void onStop() {
        super.onStop();


    }
    @Override
    public void onBackPressed(){
        finish();
        startActivity(new Intent(this,MainActivity.class)); //start main on back and finish, this is to update points always
    }

    public void showWords(View view){ //goes to the lyricsActivity
        Intent lyrics = new Intent(this, lyricsActivity.class);

        String urlLyr = "http://www.inf.ed.ac.uk/teaching/courses/selp/data/songs/" + mySpinner.getSelectedItem().toString().substring(5,7) + "/lyrics.txt"; //sends the url for the song

        lyrics.putExtra("lyricsURL", urlLyr);

        lyrics.putExtra("song", mySpinner.getSelectedItem().toString()); //sends the song like this so that its easier to get the list of found words.

        startActivity(lyrics);
    }

    public void guess(View view){ //opens the guessActivity
        Intent guessActivity = new Intent(this, GuessActivity.class);
        guessActivity.putExtra("Title", songs.get(mySpinner.getSelectedItemPosition()).getTitle());//send the title of the song for comparison
        guessActivity.putExtra("URL", songs.get(mySpinner.getSelectedItemPosition()).getLink()); //sends the url of the song to open in case the guess is correct.

        startActivity(guessActivity);

    }

    public void giveHint(View view){//gives a word provided the user has enough points

        SharedPreferences sharedPreferences = getSharedPreferences("shared_prefs", MODE_PRIVATE);

        Gson gson = new Gson();//loading in the list of found markers
        String json = sharedPreferences.getString(mySpinner.getSelectedItem().toString(), null);
        Type type = new TypeToken<ArrayList<Placemark>> () {}.getType();
        found = gson.fromJson(json, type);

        gson = new Gson();//loads in all the markers (saved in maps activity
        json = sharedPreferences.getString(mySpinner.getSelectedItem().toString() + "all", null);
        type = new TypeToken<ArrayList<Placemark>>() {}.getType();
        forHints = gson.fromJson(json, type);

        if(forHints == null){ //if hints is null, the user has not collected a point. This value is only stored when a point is collected in the maps activity
            Toast.makeText(getApplicationContext(),"Please Collect 1 word first",Toast.LENGTH_SHORT).show();
        }else if(points >= 50){ //if points are above 50 the it will give the hint
            Random r = new Random();
            int rand = r.nextInt(forHints.size());//random int

            found.add(forHints.get(rand)); //add the random marker from forHints to found
            forHints.remove(rand); //remove the random marker from forHints

            points = points - 50; //remove and displaypoints
            textView = (TextView) findViewById(R.id.songsPoints);
            textView.setText(String.valueOf(points));

            Toast.makeText(getApplicationContext(),"New Word Added",Toast.LENGTH_SHORT).show();//send a message saying the hint was given

            saveData();//saveData(makes sure you don't get the same word over and for hints
        }else{
            Toast.makeText(getApplicationContext(),"Need 50 points for a hint",Toast.LENGTH_SHORT).show(); //otherwise, the user has under 50, shows the message
        }

    }

    private static class Song { //created a song class to store songs in an arrayList
        private final String num;
        private final String artist;
        private final String title;

        public String getLink() {
            return link;
        }

        private final String link;

        private Song(String title, String num, String link, String artist) {
            this.title = title;
            this.num = num;
            this.artist = artist;
            this.link = link;
        }
        private String getTitle(){
            return title;
        }
        private String getNum() {return num;}
    }

    private class DownloadXmlTask extends AsyncTask<String, Void, String> { //asynctask to parse the xml of songs

        @Override
        protected String doInBackground(String... urls) {
            try {

                setSongs(loadXmlFromNetwork(urls[0])); // sets the songs list (declared at the start) to the parsed xml
                return "Success";
            } catch (IOException e) {
                return "Unable to load content. Check your network connection";
            } catch (XmlPullParserException e) {
                return "Error parsing XML";
            }
        }

        @Override
        protected void onPostExecute(String result) {

            songSpin = new ArrayList<String>(); //sets the spinner with the
            for(int i = 0; i < songs.size(); i++){ //makes a new list based on the number of songs.
                songSpin.add("Song " + songs.get(i).getNum());
            }
            Spinner mySpinner = (Spinner) findViewById(R.id.spinner1);
            //sets the spinner to have the correct number of songs.
            ArrayAdapter<String> myAdapter = new ArrayAdapter<String>(SongsActivity.this,
                    R.layout.spinner_layout, songSpin);
            myAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            mySpinner.setAdapter(myAdapter);


        }

    }

    private ArrayList<Song> loadXmlFromNetwork(String urlString) throws
            XmlPullParserException, IOException { //returns an ArrayLsit of songs using downloadUrl(for the input stream) and thenusing parse
        ArrayList<Song> temp = new ArrayList<Song>();
        StringBuilder result = new StringBuilder();

        InputStream stream = downloadUrl(urlString);

        temp = parse(stream);

       return temp;
    }

    private InputStream downloadUrl(String urlString) throws IOException { //simly  connects to the URL and gets the input stream.
        URL url = new URL(urlString);
        HttpURLConnection conn = (HttpURLConnection) url.openConnection();
// Also available: HttpsURLConnection
        conn.setReadTimeout(10000 /* milliseconds */);
        conn.setConnectTimeout(15000 /* milliseconds */);
        conn.setRequestMethod("GET");
        conn.setDoInput(true);
// Starts the queryw
        conn.connect();
        return conn.getInputStream();
    }

    private static final String ns = null;

    public ArrayList<Song> parse(InputStream in) throws XmlPullParserException,
            IOException { //parse, takes an input stream and returns the arraylist of songs by parsing the inputstream
        try {
            XmlPullParser parser = Xml.newPullParser();
            parser.setFeature(XmlPullParser.FEATURE_PROCESS_NAMESPACES,
                    false);
            parser.setInput(in, null);
            parser.nextTag();//set pullparser input and nexttag

            return readFeed(parser); //reads the xml given the pullparser
        } finally {
            in.close();
        }
    }

    private ArrayList<Song> readFeed(XmlPullParser parser) throws
            XmlPullParserException, IOException {
        ArrayList<Song> entries = new ArrayList<Song>();
        parser.require(XmlPullParser.START_TAG, ns, "Songs");//start at the tag Songs
        while (parser.next() != XmlPullParser.END_TAG) {
            if (parser.getEventType() != XmlPullParser.START_TAG) {
                continue;
            }
            String name = parser.getName();
// Starts by looking for the entry tag
            if (name.equals("Song")) {//if the tag is Song then do readSong(only 1 tag
                entries.add(readSong(parser));

            } else {
                skip(parser);
            }
        }
        return entries;
    }


    private Song readSong(XmlPullParser parser) throws
            XmlPullParserException, IOException {//returns the next Song in the xml
        parser.require(XmlPullParser.START_TAG, ns,"Song");
        String num = null; String title = null; String artist = null; String link = null;
        while (parser.next() != XmlPullParser.END_TAG) {
            if (parser.getEventType() != XmlPullParser.START_TAG)
                continue;
            String name = parser.getName();
            if (name.equals("Title")) {//runs a custom function for each tag
                title = readTitle(parser);
            } else if (name.equals("Artist")) {
                artist = readArtist(parser);
            } else if (name.equals("Link")) {
                link = readLink(parser);
            } else if (name.equals("Number")) {
                num = readNum(parser);
            } else {//else skip
                skip(parser);
            }
        }
        return new Song(title, num, link, artist);
    }

    private String readTitle(XmlPullParser parser) throws IOException,
            XmlPullParserException {//reading title uses readText
        parser.require(XmlPullParser.START_TAG, ns, "Title");
        String title = readText(parser);
        parser.require(XmlPullParser.END_TAG, ns, "Title");
        return title;
    }

    private String readArtist(XmlPullParser parser) throws IOException,
            XmlPullParserException {//reading artist uses readText
        parser.require(XmlPullParser.START_TAG, ns, "Artist");
        String artist = readText(parser);
                parser.require(XmlPullParser.END_TAG, ns, "Artist");
                return artist;
            }

            private String readNum(XmlPullParser parser) throws IOException,
                    XmlPullParserException {//reading num uses readText
                parser.require(XmlPullParser.START_TAG, ns, "Number");
                String num = readText(parser);
                parser.require(XmlPullParser.END_TAG, ns, "Number");
                return num;
            }

    private String readLink(XmlPullParser parser) throws IOException,
            XmlPullParserException {//reading link also uses readText
        parser.require(XmlPullParser.START_TAG, ns, "Link");
        String link = readText(parser);
        parser.require(XmlPullParser.END_TAG, ns, "Link");
        return link;

    }

    private String readText(XmlPullParser parser) throws IOException,
            XmlPullParserException {//readtext simle gets, the clases its called in require the start and end to be before and after this is called
        String result = "";
        if (parser.next() == XmlPullParser.TEXT) {
            result = parser.getText();
            parser.nextTag();
        }
        return result;
    }

    private void skip(XmlPullParser parser) throws XmlPullParserException,
            IOException {//skips any tags that are not required
        if (parser.getEventType() != XmlPullParser.START_TAG) {
            throw new IllegalStateException();
        }
        int depth = 1;
        while (depth != 0) {
            switch (parser.next()) {
                case XmlPullParser.END_TAG:
                    depth--;
                    break;
                case XmlPullParser.START_TAG:
                    depth++;
                    break;
            }
        }
    }

}
