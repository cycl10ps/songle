package com.s1508406.songle;

import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.method.ScrollingMovementMethod;
import android.util.Log;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.lang.reflect.Type;
import java.net.URL;
import java.util.ArrayList;

public class lyricsActivity extends AppCompatActivity {
    private TextView showLyrics;
    private String lyrics;
    private String wordsFound = "";
    private InputStreamReader inputStream;
    private loadLyrics loadLyrics = new loadLyrics();
    private String lyricsUrl;
    private String song;

    ArrayList<Placemark> found;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lyrics);

        lyricsUrl = getIntent().getExtras().getString("lyricsURL"); //sets to URL sent from SongActivity
        song = getIntent().getExtras().getString("song");//sets the song ^^
        loadData();
        showLyrics = (TextView) findViewById(R.id.lyricsText);
        showLyrics.setMovementMethod(new ScrollingMovementMethod());
        loadLyrics.execute(lyricsUrl); //async tast which reads the txt file and outputs the words that have been found


    }

    public void loadData(){//load data only loads the found list. its the only list needed. The string 'song' is needed for this
        SharedPreferences sharedPreferences = getSharedPreferences("shared_prefs", MODE_PRIVATE);
        Gson gson = new Gson();
        String json = sharedPreferences.getString(song, null);
        Type type = new TypeToken<ArrayList<Placemark>>() {}.getType();
        found = gson.fromJson(json, type);

        if(found == null){
            Log.d("LOGNULLTEST", "FOUND IS EMPTY");
            found = new ArrayList<Placemark>();
        }
    }

    public void loadLyric() throws IOException { //load lyrics uses a bufered reader with an input stream to set lyrics to be all thelyrics in the song
        StringBuilder stringBuilder = new StringBuilder();
        String line;
        BufferedReader bufferedReader;
        try {

            bufferedReader = new BufferedReader(inputStream);

            while ((line = bufferedReader.readLine()) != null) {
                stringBuilder.append(line + "\n");
            }

        } catch (IOException e) {
            e.printStackTrace();

        }

        lyrics = stringBuilder.toString();
        Log.d("check", String.valueOf(lyrics.length()));

    }

    private class loadLyrics extends AsyncTask<String, Void, String>{ //async start for instantiating the inputStream in the background

        @Override
        protected String doInBackground(String... string){
            String lyricsURL = string[0];
            try {
                inputStream = new InputStreamReader(new URL(lyricsURL).openStream());
            } catch (IOException e) {
                e.printStackTrace();
            }

            return "";
        }

        @Override
        protected void onPostExecute(String string) {
            try {
                loadLyric();
            } catch (IOException e) {
                e.printStackTrace();
            }
            String position;//position of the word in the song

            String[] lines = lyrics.split("\\n"); //split the song into an array by line

            for(int i = 0; i < found.size(); i++) { //loop through all the Placemarks to

                position = found.get(i).getName(); //position is the name of each marker
                int line = Integer.parseInt(position.substring(0, position.indexOf(":"))); //use substring and parseint to set line and word
                int word = Integer.parseInt(position.substring(position.indexOf(":") + 1, position.length()));

                String[] words = lines[line - 1].split(" "); //split the song further into an array by each word
                wordsFound = wordsFound + words[word-1] + "\n"; //add them all to the string wordsFound
            }
            showLyrics.setText(wordsFound); //display wordsFound.


        }
    }

}


